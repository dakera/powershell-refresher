return;

# 
# Operator Examples:
#
1 -eq 1
4 -gt 4
42 -eq "World"
5 - 2 -eq 3
$i = 5
$i -gt 3 



“Hello World” -like “*World”
“Hello World” -match “[A-Z]*”
$Matches

“Hello”,”World” –join “_“
“Hello_World” –split “_“
“Hello Cmd!” –replace “Cmd”,”PowerShell”

# Arrays
@(1..4)
@(1..4)[0]
@(1..4) | select -First 1
@(1..4)[-1]
@(1..4)[-3]
@(1..4) | select -Last 1






